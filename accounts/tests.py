from django.test import TestCase
from django.urls import resolve
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .views import login_view, logout_view, signup_view
from django.apps import apps
from .apps import AccountsConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.utils import timezone
import datetime
import unittest
import time

class accountsUnitTest(TestCase):
    def test_accounts_create_user(self):
        user = User.objects.create_user(username='kukuruyuk', password='12345')
        user.save()
        self.assertTrue(user in User.objects.all())
        

    def test_accounts_valid_input_forms_to_model(self):
        form_data = {'username': 'kukuruyuk7', 'password1': 'Otong456', 'password2': 'Otong456'}
        form = UserCreationForm(data=form_data)
        self.assertTrue(form.is_valid())
        new_user = form.save()
        self.assertEqual(new_user.username, 'kukuruyuk7')

    # def test_accounts_login_valid_input_forms_to_model(self):
    #     form_data = {'username': 'ucup', 'password': 'Admin456'}
    #     form = AuthenticationForm(data=form_data)
    #     self.assertTrue(form.is_valid())
    #     new_user = form.getUser()
    #     self.assertEqual(new_user.username, 'ucup')    

    def test_accounts_form_login(self):
        response = self.client.get('/accounts/login/', {'username': 'ucup', 'password': 'Admin456'})
        self.assertEqual(response.status_code, 200)

    def test_accounts_form_login(self):
        response = self.client.get('/accounts/signup/', {'username': 'kukuruyuk3', 'password1': '123456', 'password2': '123456'})
        self.assertEqual(response.status_code, 200)    
        
    def test_accounts_login(self):
        self.client.login(username='kukuruyuk', password='12345')
        response = self.client.get('')
        self.assertTrue("kukuruyuk", response.content.decode('utf-8'))   

    def test_accounts_logout(self):
        self.client.logout
        response = self.client.get('')
        self.assertIn("Login", response.content.decode('utf-8'))       

    def test_accounts_no_login(self):
        self.client.login(username='----', password='----')
        response = self.client.get('')
        self.assertNotIn("----", response.content.decode('utf-8'))       


    def test_accounts_login_url_is_exist(self):
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_accounts_logout_url_is_exist(self):
        response = self.client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 302)

    def test_accounts_signup_url_is_exist(self):
        response = self.client.get('/accounts/signup/')
        self.assertEqual(response.status_code, 200)    
        

    def test_accounts_url_is_notexist(self):
        response = self.client.get('/tidakada/')
        self.assertEqual(response.status_code, 404)    

    def test_accounts_login_using_login_func(self):
        found = resolve('/accounts/login/')
        self.assertEqual(found.func, login_view)

    def test_accounts_logout_using_logout_func(self):
        found = resolve('/accounts/logout/')
        self.assertEqual(found.func, logout_view)  

    def test_accounts_signup_using_signup_func(self):
        found = resolve('/accounts/signup/')
        self.assertEqual(found.func, signup_view)        

    def test_accounts_login_content_contains_greeting(self):
            response = self.client.get('/accounts/login/')
            self.assertIn("Login", response.content.decode('utf-8'))    

    def test_accounts_signup_content_contains_greeting(self):
        response = self.client.get('/accounts/signup/')
        self.assertIn("Signup", response.content.decode('utf-8'))        

    def test_accounts_using_login_template(self):
        response = self.client.get('/accounts/login/')
        self.assertTemplateUsed(response, "accounts/login.html")    

    def test_accounts_using_signup_template(self):
        response = self.client.get('/accounts/signup/')
        self.assertTemplateUsed(response, "accounts/signup.html")        

    def test_accounts_apps(self):
        self.assertEqual(AccountsConfig.name, 'accounts')
        self.assertEqual(apps.get_app_config('accounts').name, 'accounts')

class accountsFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options = chrome_options)
        super(accountsFunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()   
        super(accountsFunctionalTest, self).tearDown()

    def test_visit_accounts_login(self):
        selenium = self.selenium
        selenium.get("http://localhost:8000/accounts/login")
        time.sleep(3)
        greeting = selenium.find_element_by_id('greeting_login').text
        self.assertIn("Login", selenium.title)
        self.assertIn("Login", greeting)
        username = selenium.find_element_by_id('id_username')
        password = selenium.find_element_by_id('id_password')
        username.send_keys('admin')
        password.send_keys('admin')
        password.submit()
        time.sleep(3)
        result = selenium.find_element_by_id('greetings-user').text
        self.assertIn('Hello admin', result)
        time.sleep(3)


    def test_visit_accounts_signup(self):
        selenium = self.selenium
        selenium.get("http://localhost:8000/accounts/signup")
        time.sleep(3)
        greeting = selenium.find_element_by_id('greeting_signup').text
        self.assertIn("Signup", selenium.title)
        self.assertIn("Signup", greeting)    
        # username = selenium.find_element_by_id('id_username')
        # password1 = selenium.find_element_by_id('id_password1')
        # password2 = selenium.find_element_by_id('id_password2')
        # username.send_keys('ucup21')
        # password1.send_keys('Otong456')
        # password2.send_keys('Otong456')
        # password2.submit()
        # time.sleep(3)
        

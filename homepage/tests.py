from django.test import TestCase
from django.urls import resolve
from .views import homepage_index
from django.apps import apps
from .apps import HomepageConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.utils import timezone
import datetime
import unittest
import time

class homepageUnitTest(TestCase):
    def test_homepage_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_admin_is_exist(self):
        response = self.client.get('/admintolhastolhasyeah/')
        self.assertEqual(response.status_code, 302)

    def test_homepage_url_is_notexist(self):
        response = self.client.get('/tidakada/')
        self.assertEqual(response.status_code, 404)    

    def test_using_homepage_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage_index)

    def test_homepage_content_contains_greeting(self):
        response = self.client.get('/')
        self.assertIn("Hello!", response.content.decode('utf-8'))    
        self.assertIn("I'm Tolhas Parulian Jonathan", response.content.decode('utf-8'))    

    def test_using_homepage_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, "homepage/index.html")    

    def test_homepage_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

class homepageFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options = chrome_options)
        super(homepageFunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()   
        super(homepageFunctionalTest, self).tearDown()

    def test_visit_homepage(self):
        selenium = self.selenium
        selenium.get("http://localhost:8000/")
        time.sleep(2)
        greeting = selenium.find_element_by_id('greeting').text
        # change_theme = selenium.find_element_by_id('change-theme').text
        self.assertIn("Tolhas's Website", selenium.title)
        self.assertIn("I'm Tolhas Parulian Jonathan", greeting)
        # self.assertIn("Ubah Tema", change_theme)


